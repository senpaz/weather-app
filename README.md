# Weather App

Welcome to the weather web application that shows the weather in any city in the world

Completed on Vue 3 + Open Weather API

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
